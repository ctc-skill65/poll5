<?php

function checkLogin() {
    global $user_id, $user;

    $login_path = "/auth/login.php";
    if (!isset($_SESSION['user_id'])) {
        redirect($login_path);
    }

    $user_id = $_SESSION['user_id'];

    $user = DB::row("SELECT * FROM `users` WHERE `user_id`='{$user_id}'");
    if (empty($user) || $user['status'] !== '1') {
        redirect($login_path);
    }
}

function checkAuth($user_type) {
    global $user;

    checkLogin();

    $login_path = "/auth/login.php";

    if ($user['user_type'] !== $user_type) {
        redirect($login_path);
    }
}
