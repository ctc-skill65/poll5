<?php

class DB 
{
    public static $conn;

    public static function connect()
    {
        self::$conn = new mysqli(
            conf('db_host'),
            conf('db_user'),
            conf('db_password'),
            conf('db_name')
        );

        if (self::$conn->connect_error) {
            exit(self::$conn->connect_error);
        }

        self::$conn->set_charset('utf8mb4');
    }

    public static function query($sql)
    {
        return self::$conn->query($sql);
    }

    public static function query_multi($sql)
    {
        return self::$conn->multi_query($sql);
    }

    public static function row($sql)
    {
        $reslut = self::query($sql);
        if ($reslut == false) {
            return null;
        }

        return $reslut->fetch_assoc();
    }

    public static function result($sql)
    {
        $reslut = self::query($sql);
        if ($reslut == false) {
            return null;
        }

        $data = [];
        while ($row = $reslut->fetch_assoc()) {
            $data[] = $row;
        }

        return $data;
    }

    public static function insert($table, $data)
    {
        $keys = [];
        $values = [];

        foreach ($data as $key => $value) {
            $keys[] = "`{$key}`"; 
            $values[] = "'{$value}'"; 
        }

        $key_sql = implode(',', $keys);
        $value_sql = implode(',', $values);
        $sql = "INSERT INTO `{$table}`({$key_sql}) VALUES ({$value_sql});";

        return self::query($sql);
    }

    public static function insert_multi($table, $data)
    {
        $sql = '';
        foreach ($data as $item) {
            $keys = [];
            $values = [];

            foreach ($item as $key => $value) {
                $keys[] = "`{$key}`"; 
                $values[] = "'{$value}'"; 
            }

            $key_sql = implode(',', $keys);
            $value_sql = implode(',', $values);
            $sql .= "INSERT INTO `{$table}`({$key_sql}) VALUES ({$value_sql});";
        }

        return self::query_multi($sql);
    }

    public static function insert_id()
    {
        return self::$conn->insert_id;
    }

    public static function update($table, $data, $where)
    {
        $data_arr = [];

        foreach ($data as $key => $value) {
            $data_arr[] = "`{$key}`='{$value}'"; 
        }

        $data_sql = implode(',', $data_arr);
        $sql = "UPDATE `{$table}` SET {$data_sql} WHERE {$where}";

        return self::query($sql);
    }

    public static function delete($table, $where)
    {
        $sql = "DELETE FROM `{$table}` WHERE {$where}";

        return self::query($sql);
    }
}

DB::connect();
