<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('user');

$poll_id = get('poll');
$q_id = get('q');
$page_path = "/user/polls/edit-question.php?poll={$poll_id}&q={$q_id}";

$action = get('action');
$id = get('id');

switch ($action) {
    case 'delete':
        DB::delete('answers', "`ans_id`='{$id}'");
        break;
}

if ($action) {
    redirect($page_path);
}

if (post('ans_name')) {
    $result = DB::insert('answers', [
        'q_id' => $q_id,
        'ans_name' => post('ans_name')
    ]);

    if ($result) {
        setAlert('success', "เพิ่มคำตอบสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถเพิ่มคำตอบได้");
    }

    redirect($page_path);
}

if (post('q_name')) {
    $result = DB::update('questions', [
        'q_name' => post('q_name')
    ], "`q_id`='{$q_id}'");

    if ($result) {
        setAlert('success', "แก้ไขคำถามสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถแก้ไขคำถามได้");
    }

    redirect($page_path);
}

$items = DB::result("SELECT * FROM `answers` WHERE `q_id`='{$q_id}'");
$poll_types = DB::result("SELECT * FROM `poll_types`");
$data = DB::row("SELECT * FROM `questions` WHERE `q_id`='{$q_id}'");
ob_start();
?>
<a href="<?= url("/user/polls/edit.php?poll={$poll_id}") ?>">
    <button>< กลับ</button>
</a>

<?= showAlert() ?>
<h3>แก้ไขคำถาม</h3>
<form method="post">
    <label for="q_name">คำถาม</label>
    <input type="text" name="q_name" id="q_name" value="<?= $data['q_name'] ?>" required>

    <button type="submit">บันทึก</button>
</form>

<h3>เพิ่มคำตอบ</h3>
<form method="post">
    <label for="ans_name">คำตอบ</label>
    <input type="text" name="ans_name" id="ans_name" required>

    <button type="submit">บันทึก</button>
</form>

<h3>รายการแบบสำรวจ</h3>
<table>
    <thead>
        <tr>
            <th>รหัส</th>
            <th>คำถาม</th>
            <th>จัดการ</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($items as $item) : ?>
            <tr>
                <td><?= $item['ans_id'] ?></td>
                <td><?= $item['ans_name'] ?></td>
                <td>
                    <a href="<?= url($page_path) ?>&action=delete&id=<?= $item['ans_id'] ?>"
                    <?= clickConfirm("คุณต้องการลบ {$item['ans_name']} หรือไม่") ?>
                    >
                    ลบ
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = "แก้ไขคำถาม";
require ROOT . '/user/layout.php';
