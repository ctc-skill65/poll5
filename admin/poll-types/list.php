<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = "/admin/poll-types/list.php";

$action = get('action');
$id = get('id');

switch ($action) {
    case 'delete':
        DB::delete('poll_types', "`poll_type_id`='{$id}'");
        break;
}

if ($action) {
    redirect($page_path);
}

if ($_POST) {
    $result = DB::insert('poll_types', [
        'poll_type_name' => post('poll_type_name')
    ]);

    if ($result) {
        setAlert('success', "เพิ่มประเภทแบบสำรวจสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถเพิ่มประเภทแบบสำรวจได้");
    }

    redirect($page_path);
}

$items = DB::result("SELECT * FROM `poll_types`");
ob_start();
?>
<?= showAlert() ?>
<h3>เพิ่มประเภทแบบสำรวจ</h3>
<form method="post">
    <label for="poll_type_name">ชื่อประเภทแบบสำรวจ</label>
    <input type="text" name="poll_type_name" id="poll_type_name" required>
    <button type="submit">บันทึก</button>
</form>

<h3>รายการประเภทแบบสำรวจ</h3>
<table>
    <thead>
        <tr>
            <th>รหัส</th>
            <th>ประเภทแบบสำรวจ</th>
            <th>จัดการ</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($items as $item) : ?>
            <tr>
                <td><?= $item['poll_type_id'] ?></td>
                <td><?= $item['poll_type_name'] ?></td>
                <td>
                    <a href="<?= url("/admin/poll-types/edit.php?id={$item['poll_type_id']}") ?>">
                    แก้ไข
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="?action=delete&id=<?= $item['poll_type_id'] ?>"
                    <?= clickConfirm("คุณต้องการลบ {$item['poll_type_name']} หรือไม่") ?>
                    >
                    ลบ
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = "จัดการประเภทแบบสำรวจ";
require ROOT . '/admin/layout.php';
